<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/hiyouga/LLaMA-Factory/blob/main/assets/logo.png"><img src="/hiyouga/LLaMA-Factory/raw/main/assets/logo.png" alt="# 骆驼工厂" style="max-width: 100%;"></a></p>
<p dir="auto"><a href="https://github.com/hiyouga/LLaMA-Factory/stargazers"><img src="https://camo.githubusercontent.com/f409bbfecc31c6790f0a025fbd719c58d2f8ea7e2e8c4d5c25dfc8d48f5b6c0c/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f73746172732f6869796f7567612f4c4c614d412d466163746f72793f7374796c653d736f6369616c" alt="GitHub 存储库星星" data-canonical-src="https://img.shields.io/github/stars/hiyouga/LLaMA-Factory?style=social" style="max-width: 100%;"></a>
<a href="/hiyouga/LLaMA-Factory/blob/main/LICENSE"><img src="https://camo.githubusercontent.com/c7cedfbad29a6999832f5fb172343d25586b6979622db944c337f3be764f5b3b/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6c6963656e73652f6869796f7567612f4c4c614d412d466163746f7279" alt="GitHub 代码许可" data-canonical-src="https://img.shields.io/github/license/hiyouga/LLaMA-Factory" style="max-width: 100%;"></a>
<a href="https://github.com/hiyouga/LLaMA-Factory/commits/main"><img src="https://camo.githubusercontent.com/a2f2c81f0df6c42dcac4966556efb170ab09b5786f6a39db32c144001eb34864/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f6c6173742d636f6d6d69742f6869796f7567612f4c4c614d412d466163746f7279" alt="GitHub 最后一次提交" data-canonical-src="https://img.shields.io/github/last-commit/hiyouga/LLaMA-Factory" style="max-width: 100%;"></a>
<a href="https://pypi.org/project/llmtuner/" rel="nofollow"><img src="https://camo.githubusercontent.com/b6c8ec7f5cb95133f8f3323c41dd02cbbecb043d6bdac911c34646c57b2424b4/68747470733a2f2f696d672e736869656c64732e696f2f707970692f762f6c6c6d74756e6572" alt="皮伊" data-canonical-src="https://img.shields.io/pypi/v/llmtuner" style="max-width: 100%;"></a>
<a href="https://pypi.org/project/llmtuner/" rel="nofollow"><img src="https://camo.githubusercontent.com/86ce734df829795730715951a6f6646c7e495c1b030dd05fa690aa877fe1c5ff/68747470733a2f2f7374617469632e706570792e746563682f62616467652f6c6c6d74756e6572" alt="下载" data-canonical-src="https://static.pepy.tech/badge/llmtuner" style="max-width: 100%;"></a>
<a href="#projects-using-llama-factory"><img src="https://camo.githubusercontent.com/51bed294372155f8aa291cc179096e93cb46e3ccfd65aeadad851252002dc52b/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6369746174696f6e2d34342d677265656e" alt="引文" data-canonical-src="https://img.shields.io/badge/citation-44-green" style="max-width: 100%;"></a>
<a href="https://github.com/hiyouga/LLaMA-Factory/pulls"><img src="https://camo.githubusercontent.com/05426382430092be65f7cb9c42eb64f05cdf054848d597274c0e2f1cf6214fe0/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f5052732d77656c636f6d652d626c7565" alt="GitHub 拉取请求" data-canonical-src="https://img.shields.io/badge/PRs-welcome-blue" style="max-width: 100%;"></a>
<a href="https://discord.gg/rKfvV9r9FK" rel="nofollow"><img src="https://camo.githubusercontent.com/1ba0baeb4ab91f8c1dbaa0265319bc8d6c8b18fd2e677dbdd05cd86dc61b53fa/68747470733a2f2f646362616467652e76657263656c2e6170702f6170692f7365727665722f724b667656397239464b3f636f6d706163743d74727565267374796c653d666c6174" alt="不和谐" data-canonical-src="https://dcbadge.vercel.app/api/server/rKfvV9r9FK?compact=true&amp;style=flat" style="max-width: 100%;"></a>
<a href="https://twitter.com/llamafactory_ai" rel="nofollow"><img src="https://camo.githubusercontent.com/ccd5038ab8d37cc862d03502c53ca10170872e0e861e22481614b19fdf2f8d38/68747470733a2f2f696d672e736869656c64732e696f2f747769747465722f666f6c6c6f772f6c6c616d61666163746f72795f6169" alt="推特" data-canonical-src="https://img.shields.io/twitter/follow/llamafactory_ai" style="max-width: 100%;"></a>
<a href="https://huggingface.co/spaces/hiyouga/LLaMA-Board" rel="nofollow"><img src="https://camo.githubusercontent.com/aee20c8bf3d57328b2a301427dac32b35bc021c15c2223ac19adfee1bab7326e/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2546302539462541342539372d4f70656e253230696e2532305370616365732d626c7565" alt="空间" data-canonical-src="https://img.shields.io/badge/%F0%9F%A4%97-Open%20in%20Spaces-blue" style="max-width: 100%;"></a>
<a href="https://modelscope.cn/studios/hiyouga/LLaMA-Board" rel="nofollow"><img src="https://camo.githubusercontent.com/ce7ec6387ef15c3017082446fbb34b666cdf6bf57c4e7e043da46ae1ee9149bf/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4d6f64656c53636f70652d4f70656e253230696e25323053747564696f732d626c7565" alt="工作室" data-canonical-src="https://img.shields.io/badge/ModelScope-Open%20in%20Studios-blue" style="max-width: 100%;"></a>
<a href="https://colab.research.google.com/drive/1eRTPn37ltBbYsISy9Aw2NuI2Aq5CQrD9?usp=sharing" rel="nofollow"><img src="https://camo.githubusercontent.com/f5e0d0538a9c2972b5d413e0ace04cecd8efd828d133133933dfffec282a4e1b/68747470733a2f2f636f6c61622e72657365617263682e676f6f676c652e636f6d2f6173736574732f636f6c61622d62616467652e737667" alt="在 Colab 中打开" data-canonical-src="https://colab.research.google.com/assets/colab-badge.svg" style="max-width: 100%;"></a></p>
<p dir="auto"><a href="https://trendshift.io/repositories/4535" rel="nofollow"><img src="https://camo.githubusercontent.com/e0a93a46580948f3aba845cc8df37de8947ed37c10d3e20ef7f7c814cc0f7f00/68747470733a2f2f7472656e6473686966742e696f2f6170692f62616467652f7265706f7369746f726965732f34353335" alt="GitHub 足迹" data-canonical-src="https://trendshift.io/api/badge/repositories/4535" style="max-width: 100%;"></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">👋加入我们的</font></font><a href="/hiyouga/LLaMA-Factory/blob/main/assets/wechat.jpg"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微信</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto">[ English | <a href="/hiyouga/LLaMA-Factory/blob/main/README_zh.md">中文</a> ]</p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微调大型语言模型可以很容易......</font></font></strong></p>
<details open="" class="details-reset border rounded-2">
  <summary class="px-3 py-2">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-device-camera-video">
    <path d="M16 3.75v8.5a.75.75 0 0 1-1.136.643L11 10.575v.675A1.75 1.75 0 0 1 9.25 13h-7.5A1.75 1.75 0 0 1 0 11.25v-6.5C0 3.784.784 3 1.75 3h7.5c.966 0 1.75.784 1.75 1.75v.675l3.864-2.318A.75.75 0 0 1 16 3.75Zm-6.5 1a.25.25 0 0 0-.25-.25h-7.5a.25.25 0 0 0-.25.25v6.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-6.5ZM11 8.825l3.5 2.1v-5.85l-3.5 2.1Z"></path>
</svg>
    <span aria-label="视频说明tutorial_cn.mp4" class="m-1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">教程_in.mp4</font></font></span>
    <span class="dropdown-caret"></span>
  </summary>

  <video src="https://private-user-images.githubusercontent.com/16256802/309488063-9840a653-7e9c-41c8-ae89-7ace5698baf6.mp4?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MTU3NDMwMDQsIm5iZiI6MTcxNTc0MjcwNCwicGF0aCI6Ii8xNjI1NjgwMi8zMDk0ODgwNjMtOTg0MGE2NTMtN2U5Yy00MWM4LWFlODktN2FjZTU2OThiYWY2Lm1wND9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDA1MTUlMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwNTE1VDAzMTE0NFomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPWFhYmFhODU5ZWFiNmEyYmMyZjEyNGRlNzg1OTU0MjRiNzhjMTE4MzIwYmUxMGI5NmEzZTRkMTEzNTM0NGFhODImWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.IJPO6f1X0PNBsxWkT0LF1InhD66kAEjjTP8f3U7cqfc" data-canonical-src="https://private-user-images.githubusercontent.com/16256802/309488063-9840a653-7e9c-41c8-ae89-7ace5698baf6.mp4?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MTU3NDMwMDQsIm5iZiI6MTcxNTc0MjcwNCwicGF0aCI6Ii8xNjI1NjgwMi8zMDk0ODgwNjMtOTg0MGE2NTMtN2U5Yy00MWM4LWFlODktN2FjZTU2OThiYWY2Lm1wND9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDA1MTUlMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwNTE1VDAzMTE0NFomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPWFhYmFhODU5ZWFiNmEyYmMyZjEyNGRlNzg1OTU0MjRiNzhjMTE4MzIwYmUxMGI5NmEzZTRkMTEzNTM0NGFhODImWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.IJPO6f1X0PNBsxWkT0LF1InhD66kAEjjTP8f3U7cqfc" controls="controls" muted="muted" class="d-block rounded-bottom-2 border-top width-fit" style="max-height:640px; min-height: 200px">

  </video>
</details>

<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">选择你的路径：</font></font></p>
<ul dir="auto">
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Colab</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：</font></font><a href="https://colab.research.google.com/drive/1eRTPn37ltBbYsISy9Aw2NuI2Aq5CQrD9?usp=sharing" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">https://colab.research.google.com/drive/1eRTPn37ltBbYsISy9Aw2NuI2Aq5CQrD9?usp=sharing</font></font></a></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本机</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：请参考</font></font><a href="#getting-started"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用方法</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录</font></font></h2><a id="user-content-table-of-contents" class="anchor" aria-label="固定链接：目录" href="#table-of-contents"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="#features"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></a></li>
<li><a href="#benchmark"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基准</font></font></a></li>
<li><a href="#changelog"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">变更日志</font></font></a></li>
<li><a href="#supported-models"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持型号</font></font></a></li>
<li><a href="#supported-training-approaches"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持的培训方法</font></font></a></li>
<li><a href="#provided-datasets"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提供的数据集</font></font></a></li>
<li><a href="#requirement"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要求</font></font></a></li>
<li><a href="#getting-started"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">入门</font></font></a></li>
<li><a href="#projects-using-llama-factory"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 LLaMA Factory 的项目</font></font></a></li>
<li><a href="#license"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></a></li>
<li><a href="#citation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">引文</font></font></a></li>
<li><a href="#acknowledgement"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">致谢</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></h2><a id="user-content-features" class="anchor" aria-label="永久链接：特点" href="#features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">各种型号</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：LLAMA、LLaVA、Mistral、Mixtral-MoE、Qwen、Yi、Gemma、Baichuan、ChatGLM、Phi 等。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">综合方法</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：（连续）预训练、（多模式）监督微调、奖励建模、PPO、DPO 和 ORPO。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可扩展资源</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：32 位全调优、16 位冻结调优、16 位 LoRA 和 2/4/8 位 QLoRA 通过 AQLM/AWQ/GPTQ/LLM.int8。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高级算法</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：GaLore、BAdam、DoRA、LongLoRA、LLaMA Pro、Mixture-of-Depths、LoRA+、LoftQ 和 Agent 调整。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实用技巧</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：FlashAttention-2、Unsloth、RoPE 缩放、NEFTune 和 rsLoRA。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实验监视器</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：LlamaBoard、TensorBoard、Wandb、MLflow等</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更快的推理</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：OpenAI 风格的 API、Gradio UI 和 CLI 以及 vLLM Worker。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">基准</font></font></h2><a id="user-content-benchmark" class="anchor" aria-label="永久链接：基准" href="#benchmark"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">与 ChatGLM 的</font></font><a href="https://github.com/THUDM/ChatGLM2-6B/tree/main/ptuning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">P-Tuning</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">相比，LLaMA Factory 的 LoRA 调优可提供高达</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3.7 倍的</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">训练速度，并在广告文本生成任务上获得更好的 Rouge 分数。通过利用 4 位量化技术，LLaMA Factory 的 QLoRA 进一步提高了 GPU 内存的效率。</font></font></p>
<p dir="auto"><a target="_blank" rel="noopener noreferrer" href="/hiyouga/LLaMA-Factory/blob/main/assets/benchmark.svg"><img src="/hiyouga/LLaMA-Factory/raw/main/assets/benchmark.svg" alt="基准" style="max-width: 100%;"></a></p>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">定义</font></font></summary>
<ul dir="auto">
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">训练速度</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：训练期间每秒处理的训练样本数。 （bs=4，cutoff_len=1024）</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Rouge Score ：</font></font></strong><font style="vertical-align: inherit;"></font><a href="https://aclanthology.org/D19-1321.pdf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">广告文本生成</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">任务开发集的 Rouge-2 分数</font><font style="vertical-align: inherit;">。 （bs=4，cutoff_len=1024）</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GPU 内存</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：4 位量化训练中的峰值 GPU 内存使用情况。 （bs=1，cutoff_len=1024）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们采用</font></font><code>pre_seq_len=128</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatGLM 的 P-Tuning 和</font></font><code>lora_rank=32</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLaMA Factory 的 LoRA 调整。</font></font></li>
</ul>
</details>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">变更日志</font></font></h2><a id="user-content-changelog" class="anchor" aria-label="永久链接：变更日志" href="#changelog"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[2014-05-24] 支持在Ascend NPU设备上进行训练和推理。有关详细信息，</font><font style="vertical-align: inherit;">请检查</font></font><a href="#installation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部分。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[24/05/13] 支持Yi-1.5</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">系列机型微调</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[26/04/24] 我们支持对LLaVA-1.5</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多模式法学硕士进行微调</font><font style="vertical-align: inherit;">。请参阅</font><font style="vertical-align: inherit;">使用</font></font><a href="/hiyouga/LLaMA-Factory/blob/main/examples/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">示例。</font></font></a><font style="vertical-align: inherit;"></font></p>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">完整变更日志</font></font></summary>
<p dir="auto">[24/04/22] We provided a <strong><a href="https://colab.research.google.com/drive/1eRTPn37ltBbYsISy9Aw2NuI2Aq5CQrD9?usp=sharing" rel="nofollow">Colab notebook</a></strong> for fine-tuning the Llama-3 model on a free T4 GPU. Two Llama-3-derived models fine-tuned using LLaMA Factory are available at Hugging Face, check <a href="https://huggingface.co/shenzhi-wang/Llama3-8B-Chinese-Chat" rel="nofollow">Llama3-8B-Chinese-Chat</a> and <a href="https://huggingface.co/zhichen/Llama3-Chinese" rel="nofollow">Llama3-Chinese</a> for details.</p>
<p dir="auto">[24/04/21] We supported <strong><a href="https://arxiv.org/abs/2404.02258" rel="nofollow">Mixture-of-Depths</a></strong> according to <a href="https://github.com/astramind-ai/Mixture-of-depths">AstraMindAI's implementation</a>. See <a href="examples/README.md">examples</a> for usage.</p>
<p dir="auto">[24/04/16] We supported <strong><a href="https://arxiv.org/abs/2404.02827" rel="nofollow">BAdam</a></strong>. See <a href="examples/README.md">examples</a> for usage.</p>
<p dir="auto">[24/04/16] We supported <strong><a href="https://github.com/unslothai/unsloth">unsloth</a></strong>'s long-sequence training (Llama-2-7B-56k within 24GB). It achieves <strong>117%</strong> speed and <strong>50%</strong> memory compared with FlashAttention-2, more benchmarks can be found in <a href="https://github.com/hiyouga/LLaMA-Factory/wiki/Performance-comparison">this page</a>.</p>
<p dir="auto">[24/03/31] We supported <strong><a href="https://arxiv.org/abs/2403.07691" rel="nofollow">ORPO</a></strong>. See <a href="examples/README.md">examples</a> for usage.</p>
<p dir="auto">[24/03/21] Our paper "<a href="https://arxiv.org/abs/2403.13372" rel="nofollow">LlamaFactory: Unified Efficient Fine-Tuning of 100+ Language Models</a>" is available at arXiv!</p>
<p dir="auto">[24/03/20] We supported <strong>FSDP+QLoRA</strong> that fine-tunes a 70B model on 2x24GB GPUs. See <a href="examples/README.md">examples</a> for usage.</p>
<p dir="auto">[24/03/13] We supported <strong><a href="https://arxiv.org/abs/2402.12354" rel="nofollow">LoRA+</a></strong>. See <a href="examples/README.md">examples</a> for usage.</p>
<p dir="auto">[24/03/07] We supported gradient low-rank projection (<strong><a href="https://arxiv.org/abs/2403.03507" rel="nofollow">GaLore</a></strong>) algorithm. See <a href="examples/README.md">examples</a> for usage.</p>
<p dir="auto">[24/03/07] We integrated <strong><a href="https://github.com/vllm-project/vllm">vLLM</a></strong> for faster and concurrent inference. Try <code>infer_backend: vllm</code> to enjoy <strong>270%</strong> inference speed.</p>
<p dir="auto">[24/02/28] We supported weight-decomposed LoRA (<strong><a href="https://arxiv.org/abs/2402.09353" rel="nofollow">DoRA</a></strong>). Try <code>use_dora: true</code> to activate DoRA training.</p>
<p dir="auto">[24/02/15] We supported <strong>block expansion</strong> proposed by <a href="https://github.com/TencentARC/LLaMA-Pro">LLaMA Pro</a>. See <a href="examples/README.md">examples</a> for usage.</p>
<p dir="auto">[24/02/05] Qwen1.5 (Qwen2 beta version) series models are supported in LLaMA-Factory. Check this <a href="https://qwenlm.github.io/blog/qwen1.5/" rel="nofollow">blog post</a> for details.</p>
<p dir="auto">[24/01/18] We supported <strong>agent tuning</strong> for most models, equipping model with tool using abilities by fine-tuning with <code>dataset: glaive_toolcall</code>.</p>
<p dir="auto">[23/12/23] We supported <strong><a href="https://github.com/unslothai/unsloth">unsloth</a></strong>'s implementation to boost LoRA tuning for the LLaMA, Mistral and Yi models. Try <code>use_unsloth: true</code> argument to activate unsloth patch. It achieves <strong>170%</strong> speed in our benchmark, check <a href="https://github.com/hiyouga/LLaMA-Factory/wiki/Performance-comparison">this page</a> for details.</p>
<p dir="auto">[23/12/12] We supported fine-tuning the latest MoE model <strong><a href="https://huggingface.co/mistralai/Mixtral-8x7B-v0.1" rel="nofollow">Mixtral 8x7B</a></strong> in our framework. See hardware requirement <a href="#hardware-requirement">here</a>.</p>
<p dir="auto">[23/12/01] We supported downloading pre-trained models and datasets from the <strong><a href="https://modelscope.cn/models" rel="nofollow">ModelScope Hub</a></strong> for Chinese mainland users. See <a href="#download-from-modelscope-hub">this tutorial</a> for usage.</p>
<p dir="auto">[23/10/21] We supported <strong><a href="https://arxiv.org/abs/2310.05914" rel="nofollow">NEFTune</a></strong> trick for fine-tuning. Try <code>neftune_noise_alpha: 5</code> argument to activate NEFTune.</p>
<p dir="auto">[23/09/27] We supported <strong><math-renderer class="js-inline-math" style="display: inline" data-static-url="https://github.githubassets.com/static" data-run-id="3a743abeebfa680115f442d8cc735df8" data-catalyst=""><mjx-container style="position: relative;" jax="CHTML" class="MathJax CtxtMenu_Attached_0" tabindex="0" ctxtmenu_counter="0"><mjx-math aria-hidden="true" class="MJX-TEX"><mjx-msup><mjx-mi class="mjx-i"><mjx-c class="mjx-c1D446 TEX-I"></mjx-c></mjx-mi><mjx-script style="vertical-align: 0.363em; margin-left: 0.052em;"><mjx-mn size="s" class="mjx-n"><mjx-c class="mjx-c32"></mjx-c></mjx-mn></mjx-script></mjx-msup></mjx-math><mjx-assistive-mml display="inline" unselectable="on"><math xmlns="http://www.w3.org/1998/Math/MathML"><msup><mi>S</mi><mn>2</mn></msup></math></mjx-assistive-mml></mjx-container></math-renderer>-Attn</strong> proposed by <a href="https://github.com/dvlab-research/LongLoRA">LongLoRA</a> for the LLaMA models. Try <code>shift_attn: true</code> argument to enable shift short attention.</p>
<p dir="auto">[23/09/23] We integrated MMLU, C-Eval and CMMLU benchmarks in this repo. See <a href="examples/README.md">examples</a> for usage.</p>
<p dir="auto">[23/09/10] We supported <strong><a href="https://github.com/Dao-AILab/flash-attention">FlashAttention-2</a></strong>. Try <code>flash_attn: fa2</code> argument to enable FlashAttention-2 if you are using RTX4090, A100 or H100 GPUs.</p>
<p dir="auto">[23/08/12] We supported <strong>RoPE scaling</strong> to extend the context length of the LLaMA models. Try <code>rope_scaling: linear</code> argument in training and <code>rope_scaling: dynamic</code> argument at inference to extrapolate the position embeddings.</p>
<p dir="auto">[23/08/11] We supported <strong><a href="https://arxiv.org/abs/2305.18290" rel="nofollow">DPO training</a></strong> for instruction-tuned models. See <a href="examples/README.md">examples</a> for usage.</p>
<p dir="auto">[23/07/31] We supported <strong>dataset streaming</strong>. Try <code>streaming: true</code> and <code>max_steps: 10000</code> arguments to load your dataset in streaming mode.</p>
<p dir="auto">[23/07/29] We released two instruction-tuned 13B models at Hugging Face. See these Hugging Face Repos (<a href="https://huggingface.co/hiyouga/Llama-2-Chinese-13b-chat" rel="nofollow">LLaMA-2</a> / <a href="https://huggingface.co/hiyouga/Baichuan-13B-sft" rel="nofollow">Baichuan</a>) for details.</p>
<p dir="auto">[23/07/18] We developed an <strong>all-in-one Web UI</strong> for training, evaluation and inference. Try <code>train_web.py</code> to fine-tune models in your Web browser. Thank <a href="https://github.com/KanadeSiina">@KanadeSiina</a> and <a href="https://github.com/codemayq">@codemayq</a> for their efforts in the development.</p>
<p dir="auto">[23/07/09] We released <strong><a href="https://github.com/hiyouga/FastEdit">FastEdit</a></strong> ⚡🩹, an easy-to-use package for editing the factual knowledge of large language models efficiently. Please follow <a href="https://github.com/hiyouga/FastEdit">FastEdit</a> if you are interested.</p>
<p dir="auto">[23/06/29] We provided a <strong>reproducible example</strong> of training a chat model using instruction-following datasets, see <a href="https://huggingface.co/hiyouga/Baichuan-7B-sft" rel="nofollow">Baichuan-7B-sft</a> for details.</p>
<p dir="auto">[23/06/22] We aligned the <a href="src/api_demo.py">demo API</a> with the <a href="https://platform.openai.com/docs/api-reference/chat" rel="nofollow">OpenAI's</a> format where you can insert the fine-tuned model in <strong>arbitrary ChatGPT-based applications</strong>.</p>
<p dir="auto">[23/06/03] We supported quantized training and inference (aka <strong><a href="https://github.com/artidoro/qlora">QLoRA</a></strong>). See <a href="examples/README.md">examples</a> for usage.</p>
</details>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持型号</font></font></h2><a id="user-content-supported-models" class="anchor" aria-label="永久链接：支持的型号" href="#supported-models"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模型</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">型号尺寸</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">默认模块</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模板</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://huggingface.co/baichuan-inc" rel="nofollow">Baichuan2</a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B/13B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">W_pack</font></font></td>
<td>baichuan2</td>
</tr>
<tr>
<td><a href="https://huggingface.co/bigscience" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">盛开</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">560M/1.1B/1.7B/3B/7.1B/176B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查询键值</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/bigscience" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">布卢姆兹</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">560M/1.1B/1.7B/3B/7.1B/176B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查询键值</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/THUDM" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天GLM3</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查询键值</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聊天glm3</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/CohereForAI" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Command-R</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">35B/104B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">连贯</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/deepseek-ai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DeepSeek (教育部)</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B/16B/67B/236B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深度搜索</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/tiiuae" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">鹘</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B/40B/180B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查询键值</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">鹘</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/google" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">宝石/代码Gemma</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2B/7B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">芽</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/internlm" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实习生LM2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B/20B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">瓦克克夫</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实习生2</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/facebookresearch/llama"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通话</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B/13B/33B/65B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/meta-llama" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">拉玛-2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B/13B/70B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆驼2</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/meta-llama" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">拉玛-3</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">8B/70B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆驼3</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/llava-hf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLaVA-1.5</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B/13B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">骆马</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/mistralai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">米斯特拉尔/米斯特拉尔</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B/8x7B/8x22B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">米斯塔拉尔</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/allenai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">榆树</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1B/7B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/microsoft" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Φ1.5/2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.3B/2.7B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/microsoft" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Φ3</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3.8B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">qkv_项目</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">菲</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/Qwen" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">奎文</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.8B/7B/14B/72B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">c_attn</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">曲文</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/Qwen" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Qwen1.5（代码/MoE）</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.5B/1.8B/4B/7B/14B/32B/72B/110B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">曲文</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/bigcode" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">星编码器2</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3B/7B/15B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">-</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/xverse" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">XVERSE</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B/13B/65B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">宇宙</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/01-ai" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">做（1/1.5）</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6B/9B/34B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">做</font></font></td>
</tr>
<tr>
<td><a href="https://huggingface.co/IEITYuan" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">元</font></font></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2B/51B/102B</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">q_proj,v_proj</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">元</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-alert markdown-alert-note" dir="auto"><p class="markdown-alert-title" dir="auto"><svg class="octicon octicon-info mr-2" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8Zm8-6.5a6.5 6.5 0 1 0 0 13 6.5 6.5 0 0 0 0-13ZM6.5 7.75A.75.75 0 0 1 7.25 7h1a.75.75 0 0 1 .75.75v2.75h.25a.75.75 0 0 1 0 1.5h-2a.75.75 0 0 1 0-1.5h.25v-2h-.25a.75.75 0 0 1-.75-.75ZM8 6a1 1 0 1 1 0-2 1 1 0 0 1 0 2Z"></path></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">笔记</font></font></p><p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">默认模块</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于</font></font><code>--lora_target</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">参数，您可以使用它</font></font><code>--lora_target all</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">来指定所有可用模块以获得更好的收敛性。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于“base”模型，参数可以从</font><font style="vertical-align: inherit;">、等</font></font><code>--template</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中选择</font><font style="vertical-align: inherit;">。但请确保</font><font style="vertical-align: inherit;">为“instruct/chat”模型</font><font style="vertical-align: inherit;">使用</font><strong><font style="vertical-align: inherit;">相应的模板。</font></strong></font><code>default</code><font style="vertical-align: inherit;"></font><code>alpaca</code><font style="vertical-align: inherit;"></font><code>vicuna</code><font style="vertical-align: inherit;"></font><strong><font style="vertical-align: inherit;"></font></strong><font style="vertical-align: inherit;"></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请记住在训练和推理中</font><font style="vertical-align: inherit;">使用</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">相同的模板。</font></font></strong><font style="vertical-align: inherit;"></font></p>
</div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请参阅</font></font><a href="/hiyouga/LLaMA-Factory/blob/main/src/llmtuner/extras/constants.py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">constants.py</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以获取我们支持的模型的完整列表。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您还可以将自定义聊天模板添加到</font></font><a href="/hiyouga/LLaMA-Factory/blob/main/src/llmtuner/data/template.py"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">template.py</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持的培训方法</font></font></h2><a id="user-content-supported-training-approaches" class="anchor" aria-label="永久链接：支持的培训方法" href="#supported-training-approaches"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">方法</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">全面调校</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">冻结调谐</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">洛拉</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">QLoRA</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">预训练</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">监督微调</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">奖励模型</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PPO培训</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DPO 培训</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">奥普培训</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">✅</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提供的数据集</font></font></h2><a id="user-content-provided-datasets" class="anchor" aria-label="永久链接：提供的数据集" href="#provided-datasets"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">预训练数据集</font></font></summary>
<ul dir="auto">
<li><a href="/hiyouga/LLaMA-Factory/blob/main/data/wiki_demo.txt">Wiki Demo (en)</a></li>
<li><a href="https://huggingface.co/datasets/tiiuae/falcon-refinedweb" rel="nofollow">RefinedWeb (en)</a></li>
<li><a href="https://huggingface.co/datasets/togethercomputer/RedPajama-Data-V2" rel="nofollow">RedPajama V2 (en)</a></li>
<li><a href="https://huggingface.co/datasets/olm/olm-wikipedia-20221220" rel="nofollow">Wikipedia (en)</a></li>
<li><a href="https://huggingface.co/datasets/pleisto/wikipedia-cn-20230720-filtered" rel="nofollow">Wikipedia (zh)</a></li>
<li><a href="https://huggingface.co/datasets/EleutherAI/pile" rel="nofollow">Pile (en)</a></li>
<li><a href="https://huggingface.co/datasets/Skywork/SkyPile-150B" rel="nofollow">SkyPile (zh)</a></li>
<li><a href="https://huggingface.co/datasets/bigcode/the-stack" rel="nofollow">The Stack (en)</a></li>
<li><a href="https://huggingface.co/datasets/bigcode/starcoderdata" rel="nofollow">StarCoder (en)</a></li>
</ul>
</details>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">监督微调数据集</font></font></summary>
<ul dir="auto">
<li><a href="https://github.com/tatsu-lab/stanford_alpaca">Stanford Alpaca (en)</a></li>
<li><a href="https://github.com/ymcui/Chinese-LLaMA-Alpaca">Stanford Alpaca (zh)</a></li>
<li><a href="https://github.com/Instruction-Tuning-with-GPT-4/GPT-4-LLM">Alpaca GPT4 (en&amp;zh)</a></li>
<li><a href="/hiyouga/LLaMA-Factory/blob/main/data/identity.json">Identity (en&amp;zh)</a></li>
<li><a href="https://huggingface.co/datasets/OpenAssistant/oasst1" rel="nofollow">Open Assistant (zh)</a></li>
<li><a href="https://huggingface.co/datasets/QingyiSi/Alpaca-CoT/tree/main/Chinese-instruction-collection" rel="nofollow">ShareGPT (zh)</a></li>
<li><a href="https://huggingface.co/datasets/JosephusCheung/GuanacoDataset" rel="nofollow">Guanaco Dataset (multilingual)</a></li>
<li><a href="https://huggingface.co/datasets/BelleGroup/train_2M_CN" rel="nofollow">BELLE 2M (zh)</a></li>
<li><a href="https://huggingface.co/datasets/BelleGroup/train_1M_CN" rel="nofollow">BELLE 1M (zh)</a></li>
<li><a href="https://huggingface.co/datasets/BelleGroup/train_0.5M_CN" rel="nofollow">BELLE 0.5M (zh)</a></li>
<li><a href="https://huggingface.co/datasets/BelleGroup/generated_chat_0.4M" rel="nofollow">BELLE Dialogue 0.4M (zh)</a></li>
<li><a href="https://huggingface.co/datasets/BelleGroup/school_math_0.25M" rel="nofollow">BELLE School Math 0.25M (zh)</a></li>
<li><a href="https://huggingface.co/datasets/BelleGroup/multiturn_chat_0.8M" rel="nofollow">BELLE Multiturn Chat 0.8M (zh)</a></li>
<li><a href="https://github.com/thunlp/UltraChat">UltraChat (en)</a></li>
<li><a href="https://huggingface.co/datasets/GAIR/lima" rel="nofollow">LIMA (en)</a></li>
<li><a href="https://huggingface.co/datasets/garage-bAInd/Open-Platypus" rel="nofollow">OpenPlatypus (en)</a></li>
<li><a href="https://huggingface.co/datasets/sahil2801/CodeAlpaca-20k" rel="nofollow">CodeAlpaca 20k (en)</a></li>
<li><a href="https://huggingface.co/datasets/QingyiSi/Alpaca-CoT" rel="nofollow">Alpaca CoT (multilingual)</a></li>
<li><a href="https://huggingface.co/datasets/Open-Orca/OpenOrca" rel="nofollow">OpenOrca (en)</a></li>
<li><a href="https://huggingface.co/datasets/Open-Orca/SlimOrca" rel="nofollow">SlimOrca (en)</a></li>
<li><a href="https://huggingface.co/datasets/TIGER-Lab/MathInstruct" rel="nofollow">MathInstruct (en)</a></li>
<li><a href="https://huggingface.co/datasets/YeungNLP/firefly-train-1.1M" rel="nofollow">Firefly 1.1M (zh)</a></li>
<li><a href="https://huggingface.co/datasets/wiki_qa" rel="nofollow">Wiki QA (en)</a></li>
<li><a href="https://huggingface.co/datasets/suolyer/webqa" rel="nofollow">Web QA (zh)</a></li>
<li><a href="https://huggingface.co/datasets/zxbsmk/webnovel_cn" rel="nofollow">WebNovel (zh)</a></li>
<li><a href="https://huggingface.co/datasets/berkeley-nest/Nectar" rel="nofollow">Nectar (en)</a></li>
<li><a href="https://www.modelscope.cn/datasets/deepctrl/deepctrl-sft-data" rel="nofollow">deepctrl (en&amp;zh)</a></li>
<li><a href="https://huggingface.co/datasets/HasturOfficial/adgen" rel="nofollow">Ad Gen (zh)</a></li>
<li><a href="https://huggingface.co/datasets/totally-not-an-llm/sharegpt-hyperfiltered-3k" rel="nofollow">ShareGPT Hyperfiltered (en)</a></li>
<li><a href="https://huggingface.co/datasets/shibing624/sharegpt_gpt4" rel="nofollow">ShareGPT4 (en&amp;zh)</a></li>
<li><a href="https://huggingface.co/datasets/HuggingFaceH4/ultrachat_200k" rel="nofollow">UltraChat 200k (en)</a></li>
<li><a href="https://huggingface.co/datasets/THUDM/AgentInstruct" rel="nofollow">AgentInstruct (en)</a></li>
<li><a href="https://huggingface.co/datasets/lmsys/lmsys-chat-1m" rel="nofollow">LMSYS Chat 1M (en)</a></li>
<li><a href="https://huggingface.co/datasets/WizardLM/WizardLM_evol_instruct_V2_196k" rel="nofollow">Evol Instruct V2 (en)</a></li>
<li><a href="https://huggingface.co/datasets/glaiveai/glaive-function-calling-v2" rel="nofollow">Glaive Function Calling V2 (en)</a></li>
<li><a href="https://huggingface.co/datasets/HuggingFaceTB/cosmopedia" rel="nofollow">Cosmopedia (en)</a></li>
<li><a href="https://huggingface.co/datasets/BUAADreamer/llava-en-zh-300k" rel="nofollow">LLaVA mixed (en&amp;zh)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/oasst_de" rel="nofollow">Open Assistant (de)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/dolly-15k_de" rel="nofollow">Dolly 15k (de)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/alpaca-gpt4_de" rel="nofollow">Alpaca GPT4 (de)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/openschnabeltier_de" rel="nofollow">OpenSchnabeltier (de)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/evol-instruct_de" rel="nofollow">Evol Instruct (de)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/dolphin_de" rel="nofollow">Dolphin (de)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/booksum_de" rel="nofollow">Booksum (de)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/airoboros-3.0_de" rel="nofollow">Airoboros (de)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/ultra-chat_de" rel="nofollow">Ultrachat (de)</a></li>
</ul>
</details>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">偏好数据集</font></font></summary>
<ul dir="auto">
<li><a href="https://huggingface.co/datasets/Anthropic/hh-rlhf" rel="nofollow">HH-RLHF (en)</a></li>
<li><a href="https://github.com/Instruction-Tuning-with-GPT-4/GPT-4-LLM">GPT-4 Generated Data (en&amp;zh)</a></li>
<li><a href="https://huggingface.co/datasets/Intel/orca_dpo_pairs" rel="nofollow">Orca DPO (en)</a></li>
<li><a href="https://huggingface.co/datasets/berkeley-nest/Nectar" rel="nofollow">Nectar (en)</a></li>
<li><a href="https://huggingface.co/datasets/hiyouga/DPO-En-Zh-20k" rel="nofollow">DPO mixed (en&amp;zh)</a></li>
<li><a href="https://huggingface.co/datasets/OpenAssistant/oasst1" rel="nofollow">Open Assistant (zh)</a></li>
<li><a href="https://huggingface.co/datasets/mayflowergmbh/intel_orca_dpo_pairs_de" rel="nofollow">Orca DPO (de)</a></li>
</ul>
</details>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">某些数据集在使用前需要确认，因此我们建议使用这些命令使用您的 Hugging Face 帐户登录。</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>pip install --upgrade huggingface_hub
huggingface-cli login</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="pip install --upgrade huggingface_hub
huggingface-cli login" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">要求</font></font></h2><a id="user-content-requirement" class="anchor" aria-label="永久链接： 要求" href="#requirement"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">强制的</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最低限度</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推荐</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Python</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3.8</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3.10</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">火炬</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1.13.1</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.2.0</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">变形金刚</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4.37.2</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4.40.1</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据集</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.14.3</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.19.1</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加速</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.27.2</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.30.0</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">佩夫特</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.9.0</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.10.0</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特尔</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.8.1</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.8.6</font></font></td>
</tr>
</tbody>
</table>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">选修的</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最低限度</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推荐</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不同的</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">11.6</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">12.2</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">深速</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.10.0</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.14.0</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">位和字节</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.39.0</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.43.1</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">弗洛姆</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.4.0</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">0.4.2</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">闪光注意事项</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.3.0</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.5.8</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">硬件要求</font></font></h3><a id="user-content-hardware-requirement" class="anchor" aria-label="永久链接：硬件要求" href="#hardware-requirement"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">*</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">估计的</font></font></em></p>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">方法</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">位</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">7B</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">13B</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">30B</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">70B</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">110B</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">8x7B</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">8x22B</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">满的</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">AMP</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">120GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">240GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">600GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1200GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2000GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">900GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2400GB</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">满的</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">16</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">60GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">120GB</font></font></td>
<td>300GB</td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">600GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">900GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">400GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1200GB</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">冻结</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">16</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">20GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">40GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">80GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">200GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">360GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">160GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">400GB</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LoRA/GaLore/BAdam</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">16</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">16 GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">32GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">64GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">160GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">240GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">120GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">320GB</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">QLoRA</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">8</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">10GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">20GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">40GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">80GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">140GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">60GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">160GB</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">QLoRA</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">12GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">24GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">48GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">72GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">30GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">96GB</font></font></td>
</tr>
<tr>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">QLoRA</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">8GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">16 GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">24GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">48GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">18GB</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">48GB</font></font></td>
</tr>
</tbody>
</table>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">入门</font></font></h2><a id="user-content-getting-started" class="anchor" aria-label="永久链接：开始使用" href="#getting-started"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></h3><a id="user-content-installation" class="anchor" aria-label="永久链接：安装" href="#installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-alert markdown-alert-important" dir="auto"><p class="markdown-alert-title" dir="auto"><svg class="octicon octicon-report mr-2" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="M0 1.75C0 .784.784 0 1.75 0h12.5C15.216 0 16 .784 16 1.75v9.5A1.75 1.75 0 0 1 14.25 13H8.06l-2.573 2.573A1.458 1.458 0 0 1 3 14.543V13H1.75A1.75 1.75 0 0 1 0 11.25Zm1.75-.25a.25.25 0 0 0-.25.25v9.5c0 .138.112.25.25.25h2a.75.75 0 0 1 .75.75v2.19l2.72-2.72a.749.749 0 0 1 .53-.22h6.5a.25.25 0 0 0 .25-.25v-9.5a.25.25 0 0 0-.25-.25Zm7 2.25v2.5a.75.75 0 0 1-1.5 0v-2.5a.75.75 0 0 1 1.5 0ZM9 9a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"></path></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">重要的</font></font></p><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装是强制性的。</font></font></p>
</div>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>git clone https://github.com/hiyouga/LLaMA-Factory.git
<span class="pl-c1">cd</span> LLaMA-Factory
pip install -e .[torch,metrics]</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="git clone https://github.com/hiyouga/LLaMA-Factory.git
cd LLaMA-Factory
pip install -e .[torch,metrics]" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可用的额外依赖项：torch、metrics、deepspeed、bitsandbytes、vllm、galore、badam、gptq、awq、aqlm、qwen、modelscope、quality</font></font></p>
<div class="markdown-alert markdown-alert-tip" dir="auto"><p class="markdown-alert-title" dir="auto"><svg class="octicon octicon-light-bulb mr-2" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="M8 1.5c-2.363 0-4 1.69-4 3.75 0 .984.424 1.625.984 2.304l.214.253c.223.264.47.556.673.848.284.411.537.896.621 1.49a.75.75 0 0 1-1.484.211c-.04-.282-.163-.547-.37-.847a8.456 8.456 0 0 0-.542-.68c-.084-.1-.173-.205-.268-.32C3.201 7.75 2.5 6.766 2.5 5.25 2.5 2.31 4.863 0 8 0s5.5 2.31 5.5 5.25c0 1.516-.701 2.5-1.328 3.259-.095.115-.184.22-.268.319-.207.245-.383.453-.541.681-.208.3-.33.565-.37.847a.751.751 0 0 1-1.485-.212c.084-.593.337-1.078.621-1.489.203-.292.45-.584.673-.848.075-.088.147-.173.213-.253.561-.679.985-1.32.985-2.304 0-2.06-1.637-3.75-4-3.75ZM5.75 12h4.5a.75.75 0 0 1 0 1.5h-4.5a.75.75 0 0 1 0-1.5ZM6 15.25a.75.75 0 0 1 .75-.75h2.5a.75.75 0 0 1 0 1.5h-2.5a.75.75 0 0 1-.75-.75Z"></path></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示</font></font></p><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于</font></font><code>pip install --no-deps -e .</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解决包冲突。</font></font></p>
</div>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于 Windows 用户</font></font></summary>
<p dir="auto">If you want to enable the quantized LoRA (QLoRA) on the Windows platform, you need to install a pre-built version of <code>bitsandbytes</code> library, which supports CUDA 11.1 to 12.2, please select the appropriate <a href="https://github.com/jllllll/bitsandbytes-windows-webui/releases/tag/wheels">release version</a> based on your CUDA version.</p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>pip install https://github.com/jllllll/bitsandbytes-windows-webui/releases/download/wheels/bitsandbytes-0.41.2.post2-py3-none-win_amd64.whl</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="pip install https://github.com/jllllll/bitsandbytes-windows-webui/releases/download/wheels/bitsandbytes-0.41.2.post2-py3-none-win_amd64.whl" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto">To enable FlashAttention-2 on the Windows platform, you need to install the precompiled <code>flash-attn</code> library, which supports CUDA 12.1 to 12.2. Please download the corresponding version from <a href="https://github.com/bdashore3/flash-attention/releases">flash-attention</a> based on your requirements.</p>
</details>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于升腾NPU用户</font></font></summary>
<p dir="auto">To utilize Ascend NPU devices for (distributed) training and inference, you need to install the <strong><a href="https://gitee.com/ascend/pytorch" rel="nofollow">torch-npu</a></strong> package and the <strong><a href="https://www.hiascend.com/developer/download/community/result?module=cann" rel="nofollow">Ascend CANN Kernels</a></strong>.</p>
<table>
<thead>
<tr>
<th>Requirement</th>
<th>Minimum</th>
<th>Recommend</th>
</tr>
</thead>
<tbody>
<tr>
<td>CANN</td>
<td>8.0.RC1</td>
<td>8.0.RC1</td>
</tr>
<tr>
<td>torch</td>
<td>2.2.0</td>
<td>2.2.0</td>
</tr>
<tr>
<td>torch-npu</td>
<td>2.2.0</td>
<td>2.2.0</td>
</tr>
<tr>
<td>deepspeed</td>
<td>0.13.2</td>
<td>0.13.2</td>
</tr>
</tbody>
</table>
<p dir="auto">Remember to use <code>ASCEND_RT_VISIBLE_DEVICES</code> instead of <code>CUDA_VISIBLE_DEVICES</code> to specify the device to use.</p>
<p dir="auto">If you cannot infer model on NPU devices, try setting <code>do_sample: false</code> in the configurations.</p>
</details>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据准备</font></font></h3><a id="user-content-data-preparation" class="anchor" aria-label="永久链接：数据准备" href="#data-preparation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关数据集文件格式的详细信息，</font><font style="vertical-align: inherit;">请参阅</font></font><a href="/hiyouga/LLaMA-Factory/blob/main/data/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">data/README.md</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 。您可以使用 HuggingFace / ModelScope hub 上的数据集，也可以将数据集加载到本地磁盘中。</font></font></p>
<div class="markdown-alert markdown-alert-note" dir="auto"><p class="markdown-alert-title" dir="auto"><svg class="octicon octicon-info mr-2" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8Zm8-6.5a6.5 6.5 0 1 0 0 13 6.5 6.5 0 0 0 0-13ZM6.5 7.75A.75.75 0 0 1 7.25 7h1a.75.75 0 0 1 .75.75v2.75h.25a.75.75 0 0 1 0 1.5h-2a.75.75 0 0 1 0-1.5h.25v-2h-.25a.75.75 0 0 1-.75-.75ZM8 6a1 1 0 1 1 0-2 1 1 0 0 1 0 2Z"></path></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">笔记</font></font></p><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请更新</font></font><code>data/dataset_info.json</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以使用您的自定义数据集。</font></font></p>
</div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速开始</font></font></h3><a id="user-content-quickstart" class="anchor" aria-label="永久链接：快速入门" href="#quickstart"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用以下3条命令分别运行</font><font style="vertical-align: inherit;">Llama3-8B-Instruct模型的LoRA</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微调</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推理</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">合并。</font></font></strong><font style="vertical-align: inherit;"></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>CUDA_VISIBLE_DEVICES=0 llamafactory-cli train examples/lora_single_gpu/llama3_lora_sft.yaml
CUDA_VISIBLE_DEVICES=0 llamafactory-cli chat examples/inference/llama3_lora_sft.yaml
CUDA_VISIBLE_DEVICES=0 llamafactory-cli <span class="pl-k">export</span> examples/merge_lora/llama3_lora_sft.yaml</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="CUDA_VISIBLE_DEVICES=0 llamafactory-cli train examples/lora_single_gpu/llama3_lora_sft.yaml
CUDA_VISIBLE_DEVICES=0 llamafactory-cli chat examples/inference/llama3_lora_sft.yaml
CUDA_VISIBLE_DEVICES=0 llamafactory-cli export examples/merge_lora/llama3_lora_sft.yaml" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关高级用法（包括分布式训练），</font><font style="vertical-align: inherit;">请参阅</font></font><a href="/hiyouga/LLaMA-Factory/blob/main/examples/README.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">示例/README.md 。</font></font></a><font style="vertical-align: inherit;"></font></p>
<div class="markdown-alert markdown-alert-tip" dir="auto"><p class="markdown-alert-title" dir="auto"><svg class="octicon octicon-light-bulb mr-2" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="M8 1.5c-2.363 0-4 1.69-4 3.75 0 .984.424 1.625.984 2.304l.214.253c.223.264.47.556.673.848.284.411.537.896.621 1.49a.75.75 0 0 1-1.484.211c-.04-.282-.163-.547-.37-.847a8.456 8.456 0 0 0-.542-.68c-.084-.1-.173-.205-.268-.32C3.201 7.75 2.5 6.766 2.5 5.25 2.5 2.31 4.863 0 8 0s5.5 2.31 5.5 5.25c0 1.516-.701 2.5-1.328 3.259-.095.115-.184.22-.268.319-.207.245-.383.453-.541.681-.208.3-.33.565-.37.847a.751.751 0 0 1-1.485-.212c.084-.593.337-1.078.621-1.489.203-.292.45-.584.673-.848.075-.088.147-.173.213-.253.561-.679.985-1.32.985-2.304 0-2.06-1.637-3.75-4-3.75ZM5.75 12h4.5a.75.75 0 0 1 0 1.5h-4.5a.75.75 0 0 1 0-1.5ZM6 15.25a.75.75 0 0 1 .75-.75h2.5a.75.75 0 0 1 0 1.5h-2.5a.75.75 0 0 1-.75-.75Z"></path></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提示</font></font></p><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于</font></font><code>llamafactory-cli help</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">显示帮助信息。</font></font></p>
</div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 LLaMA Board GUI 进行微调（由</font></font><a href="https://github.com/gradio-app/gradio"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gradio</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提供支持）</font></font></h3><a id="user-content-fine-tuning-with-llama-board-gui-powered-by-gradio" class="anchor" aria-label="永久链接：使用 LLaMA Board GUI 进行微调（由 Gradio 提供支持）" href="#fine-tuning-with-llama-board-gui-powered-by-gradio"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-alert markdown-alert-important" dir="auto"><p class="markdown-alert-title" dir="auto"><svg class="octicon octicon-report mr-2" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="M0 1.75C0 .784.784 0 1.75 0h12.5C15.216 0 16 .784 16 1.75v9.5A1.75 1.75 0 0 1 14.25 13H8.06l-2.573 2.573A1.458 1.458 0 0 1 3 14.543V13H1.75A1.75 1.75 0 0 1 0 11.25Zm1.75-.25a.25.25 0 0 0-.25.25v9.5c0 .138.112.25.25.25h2a.75.75 0 0 1 .75.75v2.19l2.72-2.72a.749.749 0 0 1 .53-.22h6.5a.25.25 0 0 0 .25-.25v-9.5a.25.25 0 0 0-.25-.25Zm7 2.25v2.5a.75.75 0 0 1-1.5 0v-2.5a.75.75 0 0 1 1.5 0ZM9 9a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"></path></svg><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">重要的</font></font></p><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLaMA Board GUI 仅支持在单个 GPU 上进行训练。</font></font></p>
</div>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用本地环境</font></font></h4><a id="user-content-use-local-environment" class="anchor" aria-label="永久链接：使用本地环境" href="#use-local-environment"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>CUDA_VISIBLE_DEVICES=0 GRADIO_SHARE=1 llamafactory-cli webui</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="CUDA_VISIBLE_DEVICES=0 GRADIO_SHARE=1 llamafactory-cli webui" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于阿里云PAI或AutoDL用户</font></font></summary>
<p dir="auto">If you encountered display problems in LLaMA Board on Alibaba Cloud PAI, try using the following command to set environment variables before starting LLaMA Board:</p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">export</span> GRADIO_SERVER_PORT=7860 GRADIO_ROOT_PATH=/<span class="pl-smi">${JUPYTER_NAME}</span>/proxy/7860/</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="export GRADIO_SERVER_PORT=7860 GRADIO_ROOT_PATH=/${JUPYTER_NAME}/proxy/7860/" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto">If you are using AutoDL, please install a specific version of Gradio:</p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>pip install gradio==4.10.0</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="pip install gradio==4.10.0" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</details>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Docker</font></font></h4><a id="user-content-use-docker" class="anchor" aria-label="永久链接：使用 Docker" href="#use-docker"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>docker build -f ./Dockerfile -t llama-factory:latest <span class="pl-c1">.</span>
docker run --gpus=all \
    -v ./hf_cache:/root/.cache/huggingface/ \
    -v ./data:/app/data \
    -v ./output:/app/output \
    -e CUDA_VISIBLE_DEVICES=0 \
    -p 7860:7860 \
    --shm-size 16G \
    --name llama_factory \
    -d llama-factory:latest</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="docker build -f ./Dockerfile -t llama-factory:latest .
docker run --gpus=all \
    -v ./hf_cache:/root/.cache/huggingface/ \
    -v ./data:/app/data \
    -v ./output:/app/output \
    -e CUDA_VISIBLE_DEVICES=0 \
    -p 7860:7860 \
    --shm-size 16G \
    --name llama_factory \
    -d llama-factory:latest" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h4 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 Docker 撰写</font></font></h4><a id="user-content-use-docker-compose" class="anchor" aria-label="永久链接：使用 Docker Compose" href="#use-docker-compose"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>docker compose -f ./docker-compose.yml up -d</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="docker compose -f ./docker-compose.yml up -d" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关体积的详细信息</font></font></summary>
<ul dir="auto">
<li>hf_cache: Utilize Hugging Face cache on the host machine. Reassignable if a cache already exists in a different directory.</li>
<li>data: Place datasets on this dir of the host machine so that they can be selected on LLaMA Board GUI.</li>
<li>output: Set export dir to this location so that the merged result can be accessed directly on the host machine.</li>
</ul>
</details>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OpenAI 风格的 API 和 vLLM 进行部署</font></font></h3><a id="user-content-deploy-with-openai-style-api-and-vllm" class="anchor" aria-label="永久链接：使用 OpenAI 风格的 API 和 vLLM 进行部署" href="#deploy-with-openai-style-api-and-vllm"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>CUDA_VISIBLE_DEVICES=0,1 API_PORT=8000 llamafactory-cli api examples/inference/llama3_vllm.yaml</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="CUDA_VISIBLE_DEVICES=0,1 API_PORT=8000 llamafactory-cli api examples/inference/llama3_vllm.yaml" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从 ModelScope Hub 下载</font></font></h3><a id="user-content-download-from-modelscope-hub" class="anchor" aria-label="永久链接：从 ModelScope Hub 下载" href="#download-from-modelscope-hub"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您在从 Hugging Face 下载模型和数据集时遇到问题，可以使用 ModelScope。</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">export</span> USE_MODELSCOPE_HUB=1 <span class="pl-c"><span class="pl-c">#</span> `set USE_MODELSCOPE_HUB=1` for Windows</span></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="export USE_MODELSCOPE_HUB=1 # `set USE_MODELSCOPE_HUB=1` for Windows" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过将 ModelScope Hub 的模型 ID 指定为</font></font><code>--model_name_or_path</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.您可以在</font></font><a href="https://modelscope.cn/models" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ModelScope Hub</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上找到模型 ID 的完整列表</font><font style="vertical-align: inherit;">，例如</font></font><code>LLM-Research/Meta-Llama-3-8B-Instruct</code><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 LLaMA Factory 的项目</font></font></h2><a id="user-content-projects-using-llama-factory" class="anchor" aria-label="永久链接：使用 LLaMA Factory 的项目" href="#projects-using-llama-factory"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您有需要合并的项目，请通过电子邮件联系或创建拉取请求。</font></font></p>
<details><summary><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">点击显示</font></font></summary>
<ol dir="auto">
<li>Wang et al. ESRL: Efficient Sampling-based Reinforcement Learning for Sequence Generation. 2023. <a href="https://arxiv.org/abs/2308.02223" rel="nofollow">[arxiv]</a></li>
<li>Yu et al. Open, Closed, or Small Language Models for Text Classification? 2023. <a href="https://arxiv.org/abs/2308.10092" rel="nofollow">[arxiv]</a></li>
<li>Wang et al. UbiPhysio: Support Daily Functioning, Fitness, and Rehabilitation with Action Understanding and Feedback in Natural Language. 2023. <a href="https://arxiv.org/abs/2308.10526" rel="nofollow">[arxiv]</a></li>
<li>Luceri et al. Leveraging Large Language Models to Detect Influence Campaigns in Social Media. 2023. <a href="https://arxiv.org/abs/2311.07816" rel="nofollow">[arxiv]</a></li>
<li>Zhang et al. Alleviating Hallucinations of Large Language Models through Induced Hallucinations. 2023. <a href="https://arxiv.org/abs/2312.15710" rel="nofollow">[arxiv]</a></li>
<li>Wang et al. Know Your Needs Better: Towards Structured Understanding of Marketer Demands with Analogical Reasoning Augmented LLMs. 2024. <a href="https://arxiv.org/abs/2401.04319" rel="nofollow">[arxiv]</a></li>
<li>Wang et al. CANDLE: Iterative Conceptualization and Instantiation Distillation from Large Language Models for Commonsense Reasoning. 2024. <a href="https://arxiv.org/abs/2401.07286" rel="nofollow">[arxiv]</a></li>
<li>Choi et al. FACT-GPT: Fact-Checking Augmentation via Claim Matching with LLMs. 2024. <a href="https://arxiv.org/abs/2402.05904" rel="nofollow">[arxiv]</a></li>
<li>Zhang et al. AutoMathText: Autonomous Data Selection with Language Models for Mathematical Texts. 2024. <a href="https://arxiv.org/abs/2402.07625" rel="nofollow">[arxiv]</a></li>
<li>Lyu et al. KnowTuning: Knowledge-aware Fine-tuning for Large Language Models. 2024. <a href="https://arxiv.org/abs/2402.11176" rel="nofollow">[arxiv]</a></li>
<li>Yang et al. LaCo: Large Language Model Pruning via Layer Collaps. 2024. <a href="https://arxiv.org/abs/2402.11187" rel="nofollow">[arxiv]</a></li>
<li>Bhardwaj et al. Language Models are Homer Simpson! Safety Re-Alignment of Fine-tuned Language Models through Task Arithmetic. 2024. <a href="https://arxiv.org/abs/2402.11746" rel="nofollow">[arxiv]</a></li>
<li>Yang et al. Enhancing Empathetic Response Generation by Augmenting LLMs with Small-scale Empathetic Models. 2024. <a href="https://arxiv.org/abs/2402.11801" rel="nofollow">[arxiv]</a></li>
<li>Yi et al. Generation Meets Verification: Accelerating Large Language Model Inference with Smart Parallel Auto-Correct Decoding. 2024. <a href="https://arxiv.org/abs/2402.11809" rel="nofollow">[arxiv]</a></li>
<li>Cao et al. Head-wise Shareable Attention for Large Language Models. 2024. <a href="https://arxiv.org/abs/2402.11819" rel="nofollow">[arxiv]</a></li>
<li>Zhang et al. Enhancing Multilingual Capabilities of Large Language Models through Self-Distillation from Resource-Rich Languages. 2024. <a href="https://arxiv.org/abs/2402.12204" rel="nofollow">[arxiv]</a></li>
<li>Kim et al. Efficient and Effective Vocabulary Expansion Towards Multilingual Large Language Models. 2024. <a href="https://arxiv.org/abs/2402.14714" rel="nofollow">[arxiv]</a></li>
<li>Yu et al. KIEval: A Knowledge-grounded Interactive Evaluation Framework for Large Language Models. 2024. <a href="https://arxiv.org/abs/2402.15043" rel="nofollow">[arxiv]</a></li>
<li>Huang et al. Key-Point-Driven Data Synthesis with its Enhancement on Mathematical Reasoning. 2024. <a href="https://arxiv.org/abs/2403.02333" rel="nofollow">[arxiv]</a></li>
<li>Duan et al. Negating Negatives: Alignment without Human Positive Samples via Distributional Dispreference Optimization. 2024. <a href="https://arxiv.org/abs/2403.03419" rel="nofollow">[arxiv]</a></li>
<li>Xie and Schwertfeger. Empowering Robotics with Large Language Models: osmAG Map Comprehension with LLMs. 2024. <a href="https://arxiv.org/abs/2403.08228" rel="nofollow">[arxiv]</a></li>
<li>Wu et al. Large Language Models are Parallel Multilingual Learners. 2024. <a href="https://arxiv.org/abs/2403.09073" rel="nofollow">[arxiv]</a></li>
<li>Zhang et al. EDT: Improving Large Language Models' Generation by Entropy-based Dynamic Temperature Sampling. 2024. <a href="https://arxiv.org/abs/2403.14541" rel="nofollow">[arxiv]</a></li>
<li>Weller et al. FollowIR: Evaluating and Teaching Information Retrieval Models to Follow Instructions. 2024. <a href="https://arxiv.org/abs/2403.15246" rel="nofollow">[arxiv]</a></li>
<li>Hongbin Na. CBT-LLM: A Chinese Large Language Model for Cognitive Behavioral Therapy-based Mental Health Question Answering. 2024. <a href="https://arxiv.org/abs/2403.16008" rel="nofollow">[arxiv]</a></li>
<li>Zan et al. CodeS: Natural Language to Code Repository via Multi-Layer Sketch. 2024. <a href="https://arxiv.org/abs/2403.16443" rel="nofollow">[arxiv]</a></li>
<li>Liu et al. Extensive Self-Contrast Enables Feedback-Free Language Model Alignment. 2024. <a href="https://arxiv.org/abs/2404.00604" rel="nofollow">[arxiv]</a></li>
<li>Luo et al. BAdam: A Memory Efficient Full Parameter Training Method for Large Language Models. 2024. <a href="https://arxiv.org/abs/2404.02827" rel="nofollow">[arxiv]</a></li>
<li>Du et al. Chinese Tiny LLM: Pretraining a Chinese-Centric Large Language Model. 2024. <a href="https://arxiv.org/abs/2404.04167" rel="nofollow">[arxiv]</a></li>
<li>Ma et al. Parameter Efficient Quasi-Orthogonal Fine-Tuning via Givens Rotation. 2024. <a href="https://arxiv.org/abs/2404.04316" rel="nofollow">[arxiv]</a></li>
<li>Liu et al. Dynamic Generation of Personalities with Large Language Models. 2024. <a href="https://arxiv.org/abs/2404.07084" rel="nofollow">[arxiv]</a></li>
<li>Shang et al. How Far Have We Gone in Stripped Binary Code Understanding Using Large Language Models. 2024. <a href="https://arxiv.org/abs/2404.09836" rel="nofollow">[arxiv]</a></li>
<li>Huang et al. LLMTune: Accelerate Database Knob Tuning with Large Language Models. 2024. <a href="https://arxiv.org/abs/2404.11581" rel="nofollow">[arxiv]</a></li>
<li>Deng et al. Text-Tuple-Table: Towards Information Integration in Text-to-Table Generation via Global Tuple Extraction. 2024. <a href="https://arxiv.org/abs/2404.14215" rel="nofollow">[arxiv]</a></li>
<li>Acikgoz et al. Hippocrates: An Open-Source Framework for Advancing Large Language Models in Healthcare. 2024. <a href="https://arxiv.org/abs/2404.16621" rel="nofollow">[arxiv]</a></li>
<li>Zhang et al. Small Language Models Need Strong Verifiers to Self-Correct Reasoning. 2024. <a href="https://arxiv.org/abs/2404.17140" rel="nofollow">[arxiv]</a></li>
<li>Zhou et al. FREB-TQA: A Fine-Grained Robustness Evaluation Benchmark for Table Question Answering. 2024. <a href="https://arxiv.org/abs/2404.18585" rel="nofollow">[arxiv]</a></li>
<li><strong><a href="https://github.com/Yu-Yang-Li/StarWhisper">StarWhisper</a></strong>: A large language model for Astronomy, based on ChatGLM2-6B and Qwen-14B.</li>
<li><strong><a href="https://github.com/FudanDISC/DISC-LawLLM">DISC-LawLLM</a></strong>: A large language model specialized in Chinese legal domain, based on Baichuan-13B, is capable of retrieving and reasoning on legal knowledge.</li>
<li><strong><a href="https://github.com/thomas-yanxin/Sunsimiao">Sunsimiao</a></strong>: A large language model specialized in Chinese medical domain, based on Baichuan-7B and ChatGLM-6B.</li>
<li><strong><a href="https://github.com/WangRongsheng/CareGPT">CareGPT</a></strong>: A series of large language models for Chinese medical domain, based on LLaMA2-7B and Baichuan-13B.</li>
<li><strong><a href="https://github.com/PKU-YuanGroup/Machine-Mindset/">MachineMindset</a></strong>: A series of MBTI Personality large language models, capable of giving any LLM 16 different personality types based on different datasets and training methods.</li>
<li><strong><a href="https://huggingface.co/Nekochu/Luminia-13B-v3" rel="nofollow">Luminia-13B-v3</a></strong>: A large language model specialized in generate metadata for stable diffusion. <a href="https://huggingface.co/spaces/Nekochu/Luminia-13B_SD_Prompt" rel="nofollow">[🤗Demo]</a></li>
<li><strong><a href="https://github.com/BUAADreamer/Chinese-LLaVA-Med">Chinese-LLaVA-Med</a></strong>: A multimodal large language model specialized in Chinese medical domain, based on LLaVA-1.5-7B.</li>
</ol>
</details>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></h2><a id="user-content-license" class="anchor" aria-label="永久链接：许可证" href="#license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="/hiyouga/LLaMA-Factory/blob/main/LICENSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该存储库根据Apache-2.0 License</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获得许可</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请按照模型授权使用相应的模型权重：</font></font><a href="https://huggingface.co/baichuan-inc/Baichuan2-7B-Base/blob/main/Community%20License%20for%20Baichuan%202%20Model.pdf" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Baichuan2</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://huggingface.co/spaces/bigscience/license" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">BLOOM</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://github.com/THUDM/ChatGLM3/blob/main/MODEL_LICENSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ChatGLM3</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://cohere.com/c4ai-cc-by-nc-license" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Command-R</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://github.com/deepseek-ai/DeepSeek-LLM/blob/main/LICENSE-MODEL"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DeepSeek</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://huggingface.co/tiiuae/falcon-180B/blob/main/LICENSE.txt" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Falcon</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://ai.google.dev/gemma/terms" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gemma</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://github.com/InternLM/InternLM#license"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">InternLM2</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://github.com/facebookresearch/llama/blob/main/MODEL_CARD.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLaMA</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://ai.meta.com/llama/license/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLaMA-2 (LLaVA-1.5)</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://llama.meta.com/llama3/license/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LLaMA-3</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="/hiyouga/LLaMA-Factory/blob/main/LICENSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Mistral</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="/hiyouga/LLaMA-Factory/blob/main/LICENSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OLMo</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://huggingface.co/microsoft/phi-1_5/resolve/main/Research%20License.docx" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phi -1.5/2</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://huggingface.co/microsoft/Phi-3-mini-4k-instruct/blob/main/LICENSE" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phi-3</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://github.com/QwenLM/Qwen/blob/main/Tongyi%20Qianwen%20LICENSE%20AGREEMENT"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Qwen</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://huggingface.co/spaces/bigcode/bigcode-model-license-agreement" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">StarCoder2</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://github.com/xverse-ai/XVERSE-13B/blob/main/MODEL_LICENSE.pdf"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">XVERSE</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="https://huggingface.co/01-ai/Yi-6B/blob/main/LICENSE" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Yi</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> / </font></font><a href="/hiyouga/LLaMA-Factory/blob/main/LICENSE"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Yi-1.5</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> /</font></font><a href="https://github.com/IEIT-Yuan/Yuan-2.0/blob/main/LICENSE-Yuan"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">元</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">引文</font></font></h2><a id="user-content-citation" class="anchor" aria-label="永久链接：引文" href="#citation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果这项工作有帮助，请引用为：</font></font></p>
<div class="highlight highlight-text-bibtex notranslate position-relative overflow-auto" dir="auto"><pre><span class="pl-k">@article</span>{<span class="pl-en">zheng2024llamafactory</span>,
  <span class="pl-s">title</span>=<span class="pl-s"><span class="pl-pds">{</span>LlamaFactory: Unified Efficient Fine-Tuning of 100+ Language Models<span class="pl-pds">}</span></span>,
  <span class="pl-s">author</span>=<span class="pl-s"><span class="pl-pds">{</span>Yaowei Zheng and Richong Zhang and Junhao Zhang and Yanhan Ye and Zheyan Luo and Yongqiang Ma<span class="pl-pds">}</span></span>,
  <span class="pl-s">journal</span>=<span class="pl-s"><span class="pl-pds">{</span>arXiv preprint arXiv:2403.13372<span class="pl-pds">}</span></span>,
  <span class="pl-s">year</span>=<span class="pl-s"><span class="pl-pds">{</span>2024<span class="pl-pds">}</span></span>,
  <span class="pl-s">url</span>=<span class="pl-s"><span class="pl-pds">{</span>http://arxiv.org/abs/2403.13372<span class="pl-pds">}</span></span>
}</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="@article{zheng2024llamafactory,
  title={LlamaFactory: Unified Efficient Fine-Tuning of 100+ Language Models},
  author={Yaowei Zheng and Richong Zhang and Junhao Zhang and Yanhan Ye and Zheyan Luo and Yongqiang Ma},
  journal={arXiv preprint arXiv:2403.13372},
  year={2024},
  url={http://arxiv.org/abs/2403.13372}
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">致谢</font></font></h2><a id="user-content-acknowledgement" class="anchor" aria-label="永久链接：致谢" href="#acknowledgement"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该存储库受益于</font></font><a href="https://github.com/huggingface/peft"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PEFT</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://github.com/huggingface/trl"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TRL</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><a href="https://github.com/artidoro/qlora"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">QLoRA</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://github.com/lm-sys/FastChat"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">FastChat</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。感谢他们的精彩作品。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">明星历史</font></font></h2><a id="user-content-star-history" class="anchor" aria-label="永久链接：明星历史" href="#star-history"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><a target="_blank" rel="noopener noreferrer nofollow" href="https://camo.githubusercontent.com/745d4befd5e784d63c6f82bd9a034574441687bb02b0bbebb78e30e0e5daf7bb/68747470733a2f2f6170692e737461722d686973746f72792e636f6d2f7376673f7265706f733d6869796f7567612f4c4c614d412d466163746f727926747970653d44617465"><img src="https://camo.githubusercontent.com/745d4befd5e784d63c6f82bd9a034574441687bb02b0bbebb78e30e0e5daf7bb/68747470733a2f2f6170692e737461722d686973746f72792e636f6d2f7376673f7265706f733d6869796f7567612f4c4c614d412d466163746f727926747970653d44617465" alt="明星历史图" data-canonical-src="https://api.star-history.com/svg?repos=hiyouga/LLaMA-Factory&amp;type=Date" style="max-width: 100%;"></a></p>
</article></div>
